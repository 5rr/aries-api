FROM golang:alpine as builder

RUN apk update && apk add --no-cache git

WORKDIR /api

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .



FROM alpine:latest

RUN apk --no-cache add -ca-certificates

WORKDIR /root/

COPY --from=builder /api/main .

EXPOSE 8080

CMD ["./server"]

