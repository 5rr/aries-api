module github.com/barcleo/aries

go 1.17

require (
	github.com/99designs/gqlgen v0.14.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi/v5 v5.0.4
	github.com/gocraft/dbr/v2 v2.7.2
	github.com/hanzoai/gochimp3 v0.0.0-20210305004051-da66ea724147
	github.com/joho/godotenv v1.4.0
	github.com/lib/pq v1.10.3
	github.com/rs/cors v1.8.0
	github.com/sirupsen/logrus v1.8.1
	github.com/vektah/gqlparser/v2 v2.2.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
)

require (
	github.com/agnivade/levenshtein v1.1.1 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/mitchellh/mapstructure v1.4.2 // indirect
	golang.org/x/sys v0.0.0-20211023085530-d6a326fbbf70 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
