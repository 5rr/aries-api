package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"

	"github.com/barcleo/aries/auth"
	"github.com/barcleo/aries/graph/generated"
	"github.com/barcleo/aries/graph/model"
	"github.com/barcleo/aries/services"
	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
)

func (r *mutationResolver) CreateUser(ctx context.Context, input model.NewUser) (*model.User, error) {
	var user model.User
	user.Name = input.Name
	user.Email = input.Email
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	user.Password = string(hashedPassword)
	err = services.SaveUser(user)
	if err != nil {
		log.Info(err)
		return nil, err
	}
	token, err := auth.GenerateJWTToken(user.Email)
	if err != nil {
		return nil, err
	}
	user.AuthToken = &token
	return &user, nil
}

func (r *mutationResolver) Login(ctx context.Context, input model.Login) (*model.User, error) {
	user, err := services.GetUserbyEmail(input.Email)
	if err != nil {
		log.Info(err)
		return nil, err
	}
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(input.Password))
	if err != nil {
		log.Info(err)
		return nil, err
	}
	token, err := auth.GenerateJWTToken(user.Email)
	if err != nil {
		return nil, err
	}
	user.AuthToken = &token
	return &user, nil
}

func (r *mutationResolver) RefreshJWTToken(ctx context.Context, input *model.RefreshJWTToken) (*string, error) {
	email, err := auth.ParseJWTToken(input.Token)
	if err != nil {
		return nil, fmt.Errorf("access denied")
	}
	token, err := auth.GenerateJWTToken(email)
	if err != nil {
		return nil, err
	}
	return &token, nil
}

func (r *mutationResolver) PasswordReset(ctx context.Context, input *model.PasswordReset) (bool, error) {
	user, err := services.GetUserbyEmail(input.Email)
	if err != nil {
		log.Info(err)
		return false, err
	}
	if user.Email == input.Email {
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.DefaultCost)
		if err != nil {
			return false, err
		}
		err = services.ResetPassword(user.Email, string(hashedPassword))
		if err != nil {
			return false, err
		}
	} else {
		return false, nil
	}
	return true, nil
}

func (r *mutationResolver) AddToMailchimpAudience(ctx context.Context, input *model.MailchimpAudience) (*string, error) {
	services.AddToMailChimpAudience(input.Email)
	return &input.Email, nil
}

func (r *queryResolver) Todos(ctx context.Context) ([]*model.Todo, error) {
	user := auth.ForContext(ctx)
	log.Info("User: ", user)
	// let todo []model.Todo
	return []*model.Todo{}, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
