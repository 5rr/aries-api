package services

import (
	"github.com/barcleo/aries/config"
	"github.com/barcleo/aries/graph/model"
	"github.com/hanzoai/gochimp3"
	log "github.com/sirupsen/logrus"
)

func SaveUser(u model.User) error {
	conn, err := NewDBConn()
	if err != nil {
		log.Info("NO DB CONN: ", err)
		return err
	}
	user := model.User{Name: u.Name, Email: u.Email, Password: u.Password}
	sess := conn.NewSession(nil)
	sess.InsertInto("users").Columns("name", "email", "password").Record(&user).Exec()
	return nil
}

func GetUserbyEmail(email string) (model.User, error) {
	var user model.User
	conn, err := NewDBConn()
	if err != nil {
		log.Info("NO DB CONN: ", err)
		return model.User{}, err
	}
	sess := conn.NewSession(nil)
	sess.Select("*").From("users").Where("email=?", email).Load(&user)
	return user, nil
}

func ResetPassword(email string, password string) error {
	conn, err := NewDBConn()
	if err != nil {
		log.Info("NO DB CONN: ", err)
		return err
	}
	sess := conn.NewSession(nil)
	_, err = sess.Update("users").Set("password", password).Where("email=?", email).Exec()
	if err != nil {
		log.Info("Could not update password", err)
		return err
	}
	return nil
}

func AddToMailChimpAudience(email string) {
	listID := config.Config("MAILCHIMP_AUDIENCE_ID")
	apiKey := config.Config("MAILCHIMP_API_KEY")
	client := gochimp3.New(apiKey)
	list, err := client.GetList(listID, nil)
	if err != nil {
		log.Info("Failed to get list", listID)

	}

	// Add subscriber
	req := &gochimp3.MemberRequest{
		EmailAddress: email,
		Status:       "subscribed",
	}

	if _, err := list.CreateMember(req); err != nil {
		log.Info("Failed to subscribe", req.EmailAddress)
		// return err
	}

}
