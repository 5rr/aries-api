package services

import (
	_ "database/sql"
	"fmt"

	"github.com/barcleo/aries/config"
	"github.com/gocraft/dbr/v2"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
)

var (
	host     = config.Config("DATABASE_HOST")
	user     = config.Config("POSTGRES_USER")
	password = config.Config("POSTGRES_PASSWORD")
	db       = config.Config("POSTGRES_DB")
	// port     = config.Config("POSTGRES_PORT")
	port = 5432
)

type Service struct {
	conn *dbr.Connection
}

func NewDBConn() (*dbr.Connection, error) {
	err := godotenv.Load("../.env")
	if err != nil {
		log.Info("ERROR: ", err)
	}

	connString := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, db)
	conn, err := dbr.Open("postgres", connString, nil)
	if err != nil {
		log.Info("COULD NOT CONNECT TO DB:", err)
		return nil, err
	}
	return conn, nil
}

func NewService() (*Service, error) {
	conn, err := NewDBConn()
	if err != nil {
		log.Info("NO DB CONN: ", err)
		return nil, err
	}
	log.Info("DB CONNECTED! ")
	return &Service{conn: conn}, nil
}
