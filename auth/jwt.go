package auth

import (
	"time"

	"github.com/dgrijalva/jwt-go"
	log "github.com/sirupsen/logrus"
)

type JwtClaims struct {
	email string
	jwt.StandardClaims
}

var jwtKey = []byte("Secret")

func GenerateJWTToken(email string) (string, error) {
	expirationTime := time.Now().Add(30 * time.Second)
	claims := &JwtClaims{
		email: email,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		log.Fatal("COULD NOT CREATE TOKEN: ", err)
		return "", err
	}
	return tokenString, nil
}

func ParseJWTToken(tokenStr string) (string, error) {
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		email := claims["email"].(string)
		return email, nil
	} else {
		return "", err
	}
}
