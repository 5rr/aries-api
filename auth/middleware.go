package auth

import (
	"context"
	"net/http"

	"github.com/barcleo/aries/graph/model"
	"github.com/barcleo/aries/services"
)

var userContextKey = &contextKey{"user"}

type contextKey struct {
	email string
}

// type authRW struct {
// 	http.ResponseWriter

// }

func AuthMiddleware() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			cookie, err := r.Cookie("user")
			if cookie == nil || err == nil {
				next.ServeHTTP(rw, r)
				return
			}

			email, err := ParseJWTToken(getUserFromCookie(cookie))
			if err != nil {
				http.Error(rw, "Invalid token", http.StatusUnauthorized)
				return
			}

			user, err := services.GetUserbyEmail(email)
			if err != nil {
				next.ServeHTTP(rw, r)
				return
			}

			ctx := context.WithValue(r.Context(), userContextKey, &user)
			r = r.WithContext(ctx)
			next.ServeHTTP(rw, r)
		})
	}
}

// func AuthMiddleware() func(http.Handler) http.Handler {
// 	log.Info("Middleware")
// 	return func(next http.Handler) http.Handler {
// 		return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
// 			header := r.Header.Get("Authorization")
// 			if header == "" {
// 				next.ServeHTTP(rw, r)
// 				return
// 			}
// 			email, err := ParseJWTToken(header)
// 			if err != nil {
// 				http.Error(rw, "Invalid token", http.StatusUnauthorized)
// 				return
// 			}
// 			// user := model.User{Email: email}
// 			user, err := services.GetUserbyEmail(email)
// 			if err != nil {
// 				next.ServeHTTP(rw, r)
// 				return
// 			}
// 			ctx := context.WithValue(r.Context(), userContextKey, &user)
// 			r = r.WithContext(ctx)
// 			next.ServeHTTP(rw, r)
// 		})
// 	}
// }

func ForContext(ctx context.Context) *model.User {
	raw, _ := ctx.Value(userContextKey).(*model.User)
	return raw
}

func getUserFromCookie(cookie *http.Cookie) string {
	return cookie.Value
}
